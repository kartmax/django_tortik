from django.shortcuts import render

from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import ReviewListSerializer
from .models import Review

# Create your views here.

# создание отзыва
class ReviewCreateView(generics.CreateAPIView):
    serializer_class = ReviewListSerializer

# вывод отзывов по id продукта
class ReviesListForValProductView(APIView):
    """ Получение списка Отзывов по id Product """
    def get(self, request, id_product):
        result_list = Review.objects.filter(product_id=id_product).values('name_author', 'text', 'date_create').order_by('-id')
        if result_list:
        	# если отзывы есть
        	return Response({'result_list' : list(result_list), 'status' : 200})
        else:
        	# если отзывов нету
        	return Response({'status' : 400})

# подсчет отзывов по товару
class ReviesCounterProductIDView(APIView):
    """Получение количества отзывов по id Product"""
    def get(self, request, id_product):
        count_reviews = Review.objects.filter(product_id = id_product).count()

        return Response({'count' : count_reviews})