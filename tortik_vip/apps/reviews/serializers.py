from rest_framework import serializers
from .models import Review

class ReviewListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        # fields = '__all__'
        fields = ['name_author', 'text', 'product']