from django.contrib import admin

from .models import Review

# Register your models here.

@admin.register(Review)
class AdminReview(admin.ModelAdmin):
    list_display = ['name_author', 'text', 'product', 'date_create']