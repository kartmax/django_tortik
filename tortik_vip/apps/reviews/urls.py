from django.urls import path

from . import views

app_name = 'reviews'
urlpatterns = [
    path('review/create', views.ReviewCreateView.as_view()),
    path('review/product_<int:id_product>', views.ReviesListForValProductView.as_view()),
    path('counter/product_<int:id_product>', views.ReviesCounterProductIDView.as_view()),
]