from django.db import models

from catalog.models import Product
from django.utils import timezone

# Create your models here.

class Review(models.Model):
    name_author = models.CharField('Автор', max_length=50)
    text = models.TextField('Описание')
    date_create = models.DateField('Когда написан', default=timezone.now)
    product = models.ForeignKey(Product, verbose_name = 'Продукт', on_delete=models.CASCADE)

    def __str__(self):
        return self.text;

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
