from rest_framework import serializers
from .models import Product, Group, Subgroup

class ProductListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

class GroupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'

class SubgroupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subgroup
        fields = '__all__'
