from django.db import models

# Create your models here.

class Group(models.Model):
    alias = models.CharField('Алиас', max_length=50, unique=True)
    title_ru = models.CharField('Название', max_length=50, unique=True)
    photo = models.ImageField('Фото - (Ш370 * В250)', upload_to='group/')

    def __str__(self):
        return self.title_ru;

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'

class Subgroup(models.Model):
    alias = models.CharField('Алиас', max_length=50, unique=True)
    title_ru = models.CharField('Название', max_length=50, unique=True)
    group = models.ForeignKey(Group, verbose_name='Группа', on_delete=models.CASCADE)

    def __str__(self):
        return str('{} ({})'.format(self.title_ru, Group.objects.get(id = self.group_id)));

    class Meta:
        verbose_name = 'Подгруппа'
        verbose_name_plural = 'Подгруппы'

class Product(models.Model):
    alias = models.CharField('Алиас', max_length=50, unique=True)
    title_ru = models.CharField('Краткое название', max_length=50)
    full_title_ru = models.CharField('Полное название', max_length=100, unique=True)
    photo = models.ImageField('Фото-Превью - (Ш370 * В250)', upload_to='product/')
    subgroup = models.ForeignKey(Subgroup, verbose_name='Подгруппа', on_delete=models.PROTECT)
    draft = models.BooleanField("Черновик", default=False)

    def __str__(self):
        return self.title_ru;

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

class ProductSlide(models.Model):
    title = models.CharField('Название', max_length=50)
    alt_text = models.CharField('Описание', max_length=150, default='')
    photo = models.ImageField('Слайд', upload_to='slide/')
    product = models.ForeignKey(Product, verbose_name='Продукт', on_delete=models.CASCADE)

    def __str__(self):
        return self.title;

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды'

class ProductDescription(models.Model):    
    title = models.CharField('Название', max_length=100, default='Описание')
    description = models.TextField('Описание')
    product = models.OneToOneField(Product, verbose_name='Продукт', on_delete=models.CASCADE)

    def __str__(self):
        return self.title;

    class Meta:
        verbose_name = 'Описание'
        verbose_name_plural = 'Описание'

class ProductComposition(models.Model):
    title = models.CharField('Название', max_length=100, default='Состав')
    description = models.TextField('Состав (описание)')
    product = models.OneToOneField(Product, verbose_name='Продукт', on_delete=models.CASCADE)

    def __str__(self):
        return self.title;

    class Meta:
        verbose_name = 'Состав'
        verbose_name_plural = 'Состав'

class ProductCooking(models.Model):
    title = models.CharField('Название', max_length=100, default='Приготовление')
    description = models.TextField('Приготовление (описание)')
    product = models.OneToOneField(Product, verbose_name='Продукт', on_delete=models.CASCADE)

    def __str__(self):
        return self.title;

    class Meta:
        verbose_name = 'Приготовление'
        verbose_name_plural = 'Приготовление'