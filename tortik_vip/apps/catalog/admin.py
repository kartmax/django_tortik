from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Group, Subgroup, Product, ProductSlide, ProductDescription, ProductComposition, ProductCooking

# Register your models here.

@admin.register(Group)
class AdminGroup(admin.ModelAdmin):
    list_display = ['title_ru', 'alias', 'get_image']

    readonly_fields = ('get_image',)
    def get_image(self, obj):
        return mark_safe(f'<img src={obj.photo.url} width="150">')
    get_image.short_description = 'Фото'


@admin.register(Subgroup)
class AdminSubgroup(admin.ModelAdmin):
    list_display = ['title_ru', 'alias', 'group']

class ProductCookingInline(admin.TabularInline):
    model = ProductCooking
class ProductCompositionInline(admin.TabularInline):
    model = ProductComposition
class ProductDescriptionInline(admin.TabularInline):
    model = ProductDescription
class ProductSlideInline(admin.TabularInline):
    model = ProductSlide
    extra = 1
    readonly_fields = ['get_image',]
    def get_image(self, obj):
        return mark_safe(f'<img src={obj.photo.url} width="150">')
@admin.register(Product)
class AdminProduct(admin.ModelAdmin):
    list_display = ['full_title_ru', 'alias', 'subgroup', 'get_image', 'draft']
    list_editable = ('draft',)
    
    inlines = [ProductSlideInline, ProductDescriptionInline, ProductCompositionInline, ProductCookingInline]
    
    readonly_fields = ('get_image',)
    def get_image(self, obj):
        return mark_safe(f'<img src={obj.photo.url} width="150">')
    get_image.short_description = 'Фото-Превью'




# @admin.register(ProductSlide)
# class AdminProductSlide(admin.ModelAdmin):
#     list_display = ['title', 'alt_text', 'product', 'get_image']

#     readonly_fields = ('get_image',)
#     def get_image(self, obj):
#         return mark_safe(f'<img src={obj.photo.url} width="150">')
#     get_image.short_description = 'Фото-Слайд'


