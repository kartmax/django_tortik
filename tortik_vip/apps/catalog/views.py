from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.filters import SearchFilter
from .serializers import ProductListSerializer, GroupListSerializer, SubgroupListSerializer
from .models import Product, Group, Subgroup, ProductDescription, ProductComposition, ProductCooking

# Create your views here.

def index(request):
    return render(request, 'base.html')

class ProductCreateView(generics.CreateAPIView):
    serializer_class = ProductListSerializer

class ProductListView(generics.ListAPIView):
    """ Список всех Product """
    serializer_class = ProductListSerializer
    queryset = Product.objects.all()

    filter_backends = [SearchFilter]
    search_fields = ['full_title_ru']


class GroupListView(generics.ListAPIView):
    """ Список всех Group """
    serializer_class = GroupListSerializer
    queryset = Group.objects.all()

class SubGroupListView(generics.ListAPIView):
    """ Список всех Subgroup """
    serializer_class = SubgroupListSerializer
    queryset = Subgroup.objects.all()

class SubGroupListForValGroupView(APIView):
    """ Получение списка Subgroup по id Group """
    def get(self, request, num_group):
        result_list = Subgroup.objects.filter(group=num_group).values('id', 'alias', 'title_ru', 'group')
        return Response(list(result_list))

class ProductListForValSubgroupView(APIView):
    """ Получение списка Product по id Subgroup """
    def get(self, request, num_subgroup):
        message_not_found = 'В данный момент я наполняю эту группу. Изделия появятся здесь позже.'
        products_list = Product.objects.filter(subgroup=num_subgroup, draft=False)
        count_products = len(products_list)
        result_list = products_list.values('id', 'alias', 'title_ru', 'subgroup', 'photo')
        if len(result_list) > 0:
            return Response({'result' : result_list, 'count_products' : count_products})
        else:
            return Response({'result' : message_not_found, 'count_products' : count_products})

class ProductListForValGroupView(APIView):
    """ Получение списка Product по id Group """
    def get(self, request, num_group):
        subgroups = Group.objects.get(id=num_group).subgroup_set.all()
        result_list = []
        for s in subgroups:
            if (len(s.product_set.all()) > 0):
                result_list.append(s.product_set.all().values('id', 'alias', 'title_ru', 'full_title_ru', 'photo', 'subgroup'))
        if len(result_list) > 0:
            return Response(result_list)
        else:
            return Response('Ничего не найдено')

# class ProductDetailView(generics.RetrieveAPIView):
#     serializer_class = ProductListSerializer
#     # lookup_field = 'group'
#     queryset = Product.objects.all()

class ProductDetailView(APIView):
    """ Получение данных Product по его id """
    def get(self, request, id):
        product = Product.objects.filter(id = id).values('id', 'alias', 'title_ru', 'full_title_ru', 'photo', 'subgroup')
        
        p = Product.objects.get(id = id)

        product_slide = p.productslide_set.all().values('alt_text', 'photo')
        product_description = ProductDescription.objects.filter(product_id = id).values('description')
        product_composition = ProductComposition.objects.filter(product_id = id).values('description')
        product_cooking = ProductCooking.objects.filter(product_id = id).values('description')

        if product:
            return Response({
                    'product' : product, 
                    'slide' : product_slide, 
                    'description' : product_description,
                    'composition' : product_composition,
                    'cooking' : product_cooking
                })
        else:
            return Response('Такого изделия нету')
