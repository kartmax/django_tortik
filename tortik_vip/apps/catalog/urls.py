from django.urls import path

from . import views

app_name = 'catalog'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('product/create', views.ProductCreateView.as_view()),
    path('product/list', views.ProductListView.as_view()),
    path('group/list', views.GroupListView.as_view()),
    path('subgroup/list', views.SubGroupListView.as_view()),
    path('subgroup/group_<int:num_group>', views.SubGroupListForValGroupView.as_view()),
    path('product/subgroup_<int:num_subgroup>', views.ProductListForValSubgroupView.as_view()),
    path('product/group_<int:num_group>', views.ProductListForValGroupView.as_view()),
    path('product/id_<int:id>', views.ProductDetailView.as_view()),
]
