# Generated by Django 3.1.1 on 2020-09-04 14:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0018_auto_20200902_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='group',
            field=models.ForeignKey(default=6, on_delete=django.db.models.deletion.PROTECT, to='catalog.group', verbose_name='Группа'),
            preserve_default=False,
        ),
    ]
